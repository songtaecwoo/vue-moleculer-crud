import Vue from 'vue'
import Router from 'vue-router'
import List from '@/components/List'
import Form from '@/components/Form'

Vue.use(Router)

export default new Router({
  routes: [{
    path: '/',
    name: 'list',
    component: List
  }, {
    path: '/add',
    name: 'Input',
    component: Form
  }, {
    path: '/:id',
    name: 'form',
    component: Form
  }]
})
