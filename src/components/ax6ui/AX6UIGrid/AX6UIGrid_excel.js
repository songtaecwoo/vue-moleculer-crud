/* globals DOMParser */
import XLSX from 'xlsx'

const tableToExcel = function (table, fileName) {
  let tables = [].concat(table)
  const tableString = tables[0].replace(/\&nbsp\;/g, '')
  const wb = XLSX.utils.table_to_book(new DOMParser().parseFromString(tableString, 'text/xml'))
  XLSX.writeFile(wb, fileName)

  return true
}

export default {
  export: tableToExcel
}
